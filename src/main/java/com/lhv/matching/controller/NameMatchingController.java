package com.lhv.matching.controller;

import com.lhv.matching.domain.MatchResult;
import com.lhv.matching.service.NameMatchingService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/matching")
public class NameMatchingController {

    private final NameMatchingService nameMatchingService;

    public NameMatchingController(final NameMatchingService nameMatchingService) {
        this.nameMatchingService = nameMatchingService;
    }

    @PostMapping
    public ResponseEntity<MatchResult> matchName(@RequestParam String name) {
        return new ResponseEntity<>(nameMatchingService.matchName(name), HttpStatus.OK);
    }
}
