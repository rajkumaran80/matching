package com.lhv.matching.controller;

import com.lhv.matching.domain.SanctionedName;
import com.lhv.matching.service.SanctionedNameService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sanctionedName")
public class SanctionedNameController {

    private final SanctionedNameService sanctionedNameService;

    public SanctionedNameController(final SanctionedNameService sanctionedNameService) {
        this.sanctionedNameService = sanctionedNameService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<SanctionedName> getSanctionedName(@PathVariable Long id) {
        if (sanctionedNameService.getSanctionedName(id).isPresent()) {
            return ResponseEntity.ok(sanctionedNameService.getSanctionedName(id).get());
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<SanctionedName> addSanctionedName(@RequestBody SanctionedName sanctionedName) {
        return new ResponseEntity<>(sanctionedNameService.addSanctionedName(sanctionedName), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<SanctionedName> updateSanctionedName(@PathVariable Long id, @RequestBody SanctionedName name) {
        return ResponseEntity.ok(sanctionedNameService.updateSanctionedName(id, name));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> removeSanctionedName(@PathVariable Long id) {
        sanctionedNameService.removeSanctionedName(id);
        return ResponseEntity.noContent().build();
    }
}
