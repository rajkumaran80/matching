package com.lhv.matching.domain;

import lombok.Data;

@Data
public class MatchResult {
    private boolean matched;
    private float percentage;
}
