package com.lhv.matching.service;

import com.lhv.matching.domain.SanctionedName;
import com.lhv.matching.repository.SanctionedNameRepository;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class SanctionedNameService {

    private final SanctionedNameRepository sanctionedNameRepository;

    public SanctionedNameService(final SanctionedNameRepository sanctionedNameRepository) {
        this.sanctionedNameRepository = sanctionedNameRepository;
    }

    public Optional<SanctionedName> getSanctionedName(final Long id) {
        return sanctionedNameRepository.findById(id);
    }

    public SanctionedName addSanctionedName(final SanctionedName sanctionedName) {
        return sanctionedNameRepository.save(sanctionedName);
    }

    public SanctionedName updateSanctionedName(final Long id, final SanctionedName sanctionedName) {
        final SanctionedName existingSanctionedName = sanctionedNameRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Sanctioned name not found"));
        existingSanctionedName.setName(sanctionedName.getName());
        return sanctionedNameRepository.save(existingSanctionedName);
    }

    public void removeSanctionedName(final Long id) {
        sanctionedNameRepository.deleteById(id);
    }
}
