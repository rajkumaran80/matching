package com.lhv.matching.service;

import com.lhv.matching.domain.MatchResult;
import com.lhv.matching.domain.SanctionedName;
import com.lhv.matching.repository.SanctionedNameRepository;
import info.debatty.java.stringsimilarity.MetricLCS;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

@Service
public class NameMatchingService {

    private static final List<String> stopWords = List.of("Dr", "Mr", "Mrs", "Ms", "Miss", "Master", "to", "the", "and");

    private final MetricLCS metricLCS = new MetricLCS();

    private final SanctionedNameRepository sanctionedNameRepository;

    public NameMatchingService(SanctionedNameRepository sanctionedNameRepository) {
        this.sanctionedNameRepository = sanctionedNameRepository;
    }

    public MatchResult matchName(final String name) {
        final List<String> inputNameWords = tokeniseInput(name);

        final Iterable<SanctionedName> sanctionedNames = sanctionedNameRepository.findAll();

        final double match = match(inputNameWords, sanctionedNames);

        final MatchResult matchResult = new MatchResult();
        if (match > 0) {
            matchResult.setMatched(true);
            matchResult.setPercentage((float) (match * 100) / inputNameWords.size());
        }
        return matchResult;

    }

    private List<String> tokeniseInput(final String input) {
        final Predicate<String> isNotStopWord = word -> stopWords.stream().noneMatch(word::equalsIgnoreCase);
        return Arrays.stream(input.split(" "))
                .map(word -> word.replaceAll("[^a-zA-Z]", ""))
                .filter(isNotStopWord)
                .toList();
    }

    private double match(final List<String> inputNameWords, final Iterable<SanctionedName> sanctionedNames) {

        double maxMatch = 0;
        for (final SanctionedName sanctionedName : sanctionedNames) {
            final Supplier<Stream<String>> sanctionNameWordSupplier = () -> Arrays.stream(sanctionedName.getName().split(" "));

            double matchCount = 0;
            for (final String word : inputNameWords) {
                double distance = sanctionNameWordSupplier.get().mapToDouble(sanctionNameWord -> metricLCS.distance(sanctionNameWord.toLowerCase(), word.toLowerCase())).min().orElse(1);
                if (distance > 0.5) {
                    if (sanctionNameWordSupplier.get().anyMatch(sanctionNameWord -> sanctionNameWord.contains(word))) {
                        distance = 0.5;
                    } else {
                        distance = 1;
                    }
                }
                matchCount = matchCount + 1 - distance;
            }

            if (matchCount > maxMatch) {
                maxMatch = matchCount;
            }
        }
        return maxMatch;
    }
}
