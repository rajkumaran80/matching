package com.lhv.matching.repository;

import com.lhv.matching.domain.SanctionedName;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SanctionedNameRepository extends CrudRepository<SanctionedName, Long> {
}
