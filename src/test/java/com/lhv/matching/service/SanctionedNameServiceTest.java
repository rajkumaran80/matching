package com.lhv.matching.service;

import com.lhv.matching.domain.SanctionedName;
import com.lhv.matching.repository.SanctionedNameRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SanctionedNameServiceTest {

    private final SanctionedNameRepository sanctionedNameRepository = mock(SanctionedNameRepository.class);
    private SanctionedNameService sanctionedNameService;

    @BeforeEach
    public void init() {
        sanctionedNameService = new SanctionedNameService(sanctionedNameRepository);
    }

    @Test
    public void getSanctionedNameSuccessfully() {
        final SanctionedName sanctionedName = new SanctionedName();
        sanctionedName.setId(1L);
        sanctionedName.setName("Test");
        when(sanctionedNameRepository.findById(any())).thenReturn(Optional.of(sanctionedName));

        final Optional<SanctionedName> sanctionedNameRetrieved = sanctionedNameService.getSanctionedName(1L);
        assertNotNull(sanctionedNameRetrieved);
        assertTrue(sanctionedNameRetrieved.isPresent());
        assertEquals("Test", sanctionedNameRetrieved.get().getName());
        verify(sanctionedNameRepository).findById(1L);
    }

    @Test
    public void getSanctionedNameReturnsEmptyResult() {
        when(sanctionedNameRepository.findById(any())).thenReturn(Optional.empty());

        final Optional<SanctionedName> sanctionedNameRetrieved = sanctionedNameService.getSanctionedName(1L);
        assertNotNull(sanctionedNameRetrieved);
        assertFalse(sanctionedNameRetrieved.isPresent());
        verify(sanctionedNameRepository).findById(1L);
    }

    @Test
    public void addSanctionedNameSuccessfully() {
        final SanctionedName sanctionedName = new SanctionedName();
        sanctionedName.setId(1L);
        sanctionedName.setName("Test");
        when(sanctionedNameRepository.save(any())).thenReturn(sanctionedName);

        final SanctionedName sanctionedNameToAdd = new SanctionedName();
        sanctionedNameToAdd.setName("Test");
        final SanctionedName sanctionedNameAdded = sanctionedNameService.addSanctionedName(sanctionedNameToAdd);
        assertNotNull(sanctionedNameAdded);
        assertEquals("Test", sanctionedNameAdded.getName());
        verify(sanctionedNameRepository).save(sanctionedNameToAdd);
    }

    @Test
    public void updateSanctionedNameSuccessfully() {
        final SanctionedName existingSanctionedName = new SanctionedName();
        existingSanctionedName.setId(1L);
        existingSanctionedName.setName("Test");
        when(sanctionedNameRepository.findById(any())).thenReturn(Optional.of(existingSanctionedName));

        final SanctionedName updatedSanctionedName = new SanctionedName();
        updatedSanctionedName.setId(1L);
        updatedSanctionedName.setName("Test Updated");
        when(sanctionedNameRepository.save(any())).thenReturn(updatedSanctionedName);

        final SanctionedName sanctionedNameToUpdate = new SanctionedName();
        sanctionedNameToUpdate.setName("Test Updated");
        final SanctionedName sanctionedNameUpdated = sanctionedNameService.updateSanctionedName(1L, sanctionedNameToUpdate);
        assertNotNull(sanctionedNameUpdated);
        assertEquals("Test Updated", sanctionedNameUpdated.getName());
        verify(sanctionedNameRepository).save(sanctionedNameUpdated);

    }

    @Test
    public void updateSanctionedNameFailedIfNotFound() {
        final SanctionedName existingSanctionedName = new SanctionedName();
        existingSanctionedName.setId(1L);
        existingSanctionedName.setName("Test");
        when(sanctionedNameRepository.findById(any())).thenReturn(Optional.empty());

        final SanctionedName sanctionedNameToUpdate = new SanctionedName();
        sanctionedNameToUpdate.setName("Test Updated");
        final NoSuchElementException exception = assertThrowsExactly(NoSuchElementException.class, () -> sanctionedNameService.updateSanctionedName(1L, sanctionedNameToUpdate));
        assertEquals("Sanctioned name not found", exception.getMessage());
    }

    @Test
    public void removeSanctionedNameSuccessfully() {
        final SanctionedName existingSanctionedName = new SanctionedName();
        existingSanctionedName.setId(1L);
        existingSanctionedName.setName("Test");
        when(sanctionedNameRepository.findById(any())).thenReturn(Optional.of(existingSanctionedName));

        final SanctionedName sanctionedNameToUpdate = new SanctionedName();
        sanctionedNameToUpdate.setName("Test Updated");
        sanctionedNameService.removeSanctionedName(1L);
        verify(sanctionedNameRepository).deleteById(1L);
    }

}
