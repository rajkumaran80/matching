package com.lhv.matching.service;

import com.lhv.matching.domain.MatchResult;
import com.lhv.matching.domain.SanctionedName;
import com.lhv.matching.repository.SanctionedNameRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NameMatchingServiceTest {

    private final SanctionedNameRepository sanctionedNameRepository = mock(SanctionedNameRepository.class);
    private NameMatchingService nameMatchingService;

    @BeforeEach
    public void init() {
        nameMatchingService = new NameMatchingService(sanctionedNameRepository);
    }

    @Test
    public void matchNameCompletely() {
        when(sanctionedNameRepository.findAll()).thenReturn(
                List.of(generateSanctionedName(1L, "John Smith"),
                        generateSanctionedName(1L, "Peter John")));

        final MatchResult matchResult = nameMatchingService.matchName("John Smith");
        assertNotNull(matchResult);
        assertTrue(matchResult.isMatched());
        assertEquals(100, matchResult.getPercentage());
    }

    @Test
    public void matchSomeNameWords() {
        when(sanctionedNameRepository.findAll()).thenReturn(
                List.of(generateSanctionedName(1L, "John Smith"),
                        generateSanctionedName(1L, "Peter John")));

        final MatchResult matchResult = nameMatchingService.matchName("John David");
        assertNotNull(matchResult);
        assertTrue(matchResult.isMatched());
        assertEquals(50, matchResult.getPercentage());
    }

    @Test
    public void matchNameWithNoiseWords() {
        when(sanctionedNameRepository.findAll()).thenReturn(
                List.of(generateSanctionedName(1L, "John Smith"),
                        generateSanctionedName(1L, "Peter John")));

        MatchResult matchResult = nameMatchingService.matchName("Mr. John Smith");
        assertNotNull(matchResult);
        assertTrue(matchResult.isMatched());
        assertEquals(100, matchResult.getPercentage());

        matchResult = nameMatchingService.matchName("To the Mr. John and Smith");
        assertNotNull(matchResult);
        assertTrue(matchResult.isMatched());
        assertEquals(100, matchResult.getPercentage());
    }

    @Test
    public void matchNameWithSpellingError() {
        when(sanctionedNameRepository.findAll()).thenReturn(
                List.of(generateSanctionedName(1L, "Madis"),
                        generateSanctionedName(1L, "Peter John")));

        final MatchResult matchResult = nameMatchingService.matchName("madus");
        assertNotNull(matchResult);
        assertTrue(matchResult.isMatched());
        assertTrue(matchResult.getPercentage() > 50);
    }

    @Test
    public void subStringMatch() {
        when(sanctionedNameRepository.findAll()).thenReturn(
                List.of(generateSanctionedName(1L, "Stevenson Jim"),
                        generateSanctionedName(1L, "Peter John")));

        final MatchResult matchResult = nameMatchingService.matchName("son");
        assertNotNull(matchResult);
        assertTrue(matchResult.isMatched());
        assertEquals(50, matchResult.getPercentage());
    }

    @Test
    public void nameDoesNotMatch() {
        when(sanctionedNameRepository.findAll()).thenReturn(
                List.of(generateSanctionedName(1L, "Osama Bin Laden"),
                        generateSanctionedName(1L, "Peter John")));

        final MatchResult matchResult = nameMatchingService.matchName("Paul");
        assertNotNull(matchResult);
        assertFalse(matchResult.isMatched());
        assertEquals(0, matchResult.getPercentage());
    }

    private SanctionedName generateSanctionedName(final Long id, final String name) {
        final SanctionedName sanctionedName = new SanctionedName();
        sanctionedName.setId(id);
        sanctionedName.setName(name);
        return sanctionedName;
    }
}
